Route53 Scalr Webhook Webhook App
=================================

**WILL SOON BE OBSOLETE! Scalr has their own implementation now.**

https://scalr-wiki.atlassian.net/wiki/display/docs/Integrate+Route53+with+Scalr+using+Lambda?src=contextnavpagetreemode

https://github.com/scalr-tutorials/scalr-lambda-route53

Rudimentary python request handler for adding/deleting Route53 records from Scalr webhooks.

This app expects the following in the Scalr webhook payload data:

 + R53_ZONE: name of Route53 hosted zone, e.g. customdomain.com
 + R53_ZONE_ID: Route53 hosted zone ID
 + R53_AWS_ACCESS_KEY_ID: AWS access ID
 + R53_AWS_SECRET_ACCESS_KEY: AWS access secret key
 + SCALR_EVENT_NAME: provided by Scalr
 + SCALR_EVENT_SERVER_HOSTNAME: provided by Scalr
 + SCALR_CLOUD_SERVER_ID: provided by Scalr
 + SCALR_EXTERNAL_IP: provided by Scalr

If R53_ZONE, R53_ZONE_ID, and SCALR_EXTERNAL_IP are not blank, this script will create/update an
A record pointing SCALR_EXTERNAL_IP to one of the following values:

 + SCALR_EVENT_SERVER_HOSTNAME if that values ends with the domain specified in R53_ZONE
 + SCALR_CLOUD_SERVER_ID.R53_ZONE otherwise

Note this script assumes any custom value discovered for SCALR_EVENT_SERVER_HOSTNAME (as 
specified in Scalr as Server Hostname Format) is a FQDN.

On HostDown events, this script will try to remove the A record.

The application integrates the following:

  + Signature validation
  + Request parsing

Copied from https://github.com/scalr-tutorials/webhooks-python-example

Install as Service on CentOS 7
==============================

TODO: Fix gunicorn syslog, not working.

Create dedicated user and copy Python app there:
```
$ sudo useradd -s /usr/sbin/nologin gunicorn
$ sudo git clone https://bitbucket.org/NRGpuppet/scalr_route53_updater /home/gunicorn/route53_updater
$ sudo chown -R gunicorn.gunicorn /home/gunicorn/route53_updater
```

Create startup environment /etc/sysconfig/route53_updater with the SIGNING_KEY
created for Scalr webhook:
```
SIGNING_KEY=...
```

Create systemd service definition /etc/systemd/system/route53_updater.service:
```
[Unit]
Description=Gunicorn instance to serve route53_updater
After=network.target

[Service]
PIDFile=/home/gunicorn/gunicorn.pid
Type=forking
User=gunicorn
Group=gunicorn
WorkingDirectory=/home/gunicorn/route53_updater
EnvironmentFile=-/etc/sysconfig/route53_updater
ExecStart=/usr/bin/gunicorn -D -p /home/gunicorn/gunicorn.pid --log-file /var/log/gunicorn/route53_updater.log --capture-output --bind 0.0.0.0:5000 app:app
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID

[Install]
WantedBy=multi-user.target
```

Create log directory.
```
$ sudo mkdir /var/log/gunicorn/
$ sudo chown gunicorn.gunicorn /var/log/gunicorn/
```

Finally, enable and start the new route53_updater service.
```
$ sudo systemctl start route53_updater
$ sudo systemctl enable route53_updater
```

Reference:
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-centos-7

Usage
=====

To deploy the app, you can:

   + Add it to an instance you own, install its dependencies
   + Use a PaaS, either deployed on your own infrastructure (CloudFoundry), or on public infrastructure (e.g. Heroku)


Installing Dependencies
-----------------------

To install the app's dependencies, do the following:

  + Ensure that you have the [Python Pip installer][0] available on your system
  + Run `pip install -r requirements.txt`, from the root of the project


Configuring the app
-------------------

The application is configured by passing its configuration through environment variables (i.e. it's a [12-factor app][10]).

If you are using Honcho or Foreman (as suggested below), you can simply input those environment variables in the `.env`
file.

The following environment variable is used:

  + `SIGNING_KEY`: This should be the signing key provided by Scalr, used to authenticate requests.


Running the app
---------------

Once you have installed and configured the app, you can launch it by running `honcho start web`. The app will listen on
port `5000` by default.

If you're launching the app on an instance, you'll probably want to daemonize it. To do so, navigate to the app
directory, and then run:

    gunicorn --daemon --bind 0.0.0.0:5000 app:app

Bear in mind that `gunicorn` will not read your `.env` file, so you have to pass the `SIGNING_KEY` environment variable
through other means.

One option is:

    SIGNING_KEY=yyyy gunicorn --daemon --bind 0.0.0.0:5000 app:app

To run gunicorn on the console (i.e. for test/debugging), stop CentOS 7 service created above, cd to a cloned repo of this repo (e.g. /home/gunicorn/route53_updater) and launch gunicorn manually:

    SIGNING_KEY=yyyy /usr/bin/gunicorn --log-level debug --bind 0.0.0.0:5000 app:app

For further information, you should check the [Gunicorn documentation][3].


Further reading
===============

For more information, you should refer to our [Webhooks Documentation][1].


Issues
======

Please report issues and ask questions on the project's Github repository, in the [issues section][2].


License
=======

View `LICENSE`.


  [0]: http://www.pip-installer.org/
  [10]: http://12factor.net/
  [1]: https://scalr-wiki.atlassian.net/wiki/x/FYBe
  [2]: https://github.com/scalr-tutorials/webhooks-python-example/issues
  [3]: http://gunicorn-docs.readthedocs.org/en/latest/configure.html
