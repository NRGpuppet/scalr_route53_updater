#coding:utf-8

# Rudimentary python request handler for updating Route53 records from Scalr webhooks.
#
# If R53_ZONE, R53_ZONE_ID, and SCALR_EXTERNAL_IP are not blank, this script will create/update
# an A record pointing SCALR_EXTERNAL_IP to one of the following values:
#
#  - SCALR_EVENT_SERVER_HOSTNAME if that values ends with the domain specified in R53_ZONE
#  - SCALR_CLOUD_SERVER_ID.R53_ZONE otherwise
#
# Note this script assumes any custom value discovered for SCALR_EVENT_SERVER_HOSTNAME (as 
# specified in Scalr as Server Hostname Format) is a FQDN.

import os
import hashlib
import hmac
import logging
import time

import flask
import util
import route53

SIGNING_KEY_ENV_VAR = "SIGNING_KEY"

app = flask.Flask(__name__)
app.config.update(
    signing_key=os.environ.get(SIGNING_KEY_ENV_VAR, ""),
)

# Logging configuration
stderr_log_handler = logging.StreamHandler()
app.logger.addHandler(stderr_log_handler)
app.logger.setLevel(logging.INFO)

def get_logger_timestamp():
    return "[" + time.strftime("%Y-%m-%d %H:%M:%S +0000", time.gmtime()) + "]"


@app.route("/", methods=("GET",))
def webhook_get_handler():
    return flask.Response(status=200)


@app.route("/", methods=("POST",))
def webhook_post_handler():
    payload = flask.request.json
    app.logger.info("%s Received Notification '%s' for: '%s' on '%s'", get_logger_timestamp(),
                    payload["eventId"], payload["eventName"], payload["data"]["SCALR_SERVER_ID"])

    # Verify payload data exist and are not blank
    if ("R53_ZONE" in payload["data"].keys() and
        "R53_ZONE_ID" in payload["data"].keys() and
        "R53_AWS_ACCESS_KEY_ID" in payload["data"].keys() and 
	"R53_AWS_SECRET_ACCESS_KEY" in payload["data"].keys() and
        "SCALR_EVENT_SERVER_HOSTNAME" in payload["data"].keys() and
        "SCALR_EXTERNAL_IP" in payload["data"].keys() and
        "SCALR_CLOUD_SERVER_ID" in payload["data"].keys() and
        "SCALR_EVENT_NAME" in payload["data"].keys() and
        payload["data"]["R53_ZONE"] and
        payload["data"]["R53_ZONE_ID"] and
        payload["data"]["R53_AWS_ACCESS_KEY_ID"] and
        payload["data"]["R53_AWS_SECRET_ACCESS_KEY"] and
        payload["data"]["SCALR_EVENT_SERVER_HOSTNAME"] and
        payload["data"]["SCALR_EXTERNAL_IP"] and
        payload["data"]["SCALR_CLOUD_SERVER_ID"] and
        payload["data"]["SCALR_EVENT_NAME"]):

        # Trigger on the events we care about
        if (payload["data"]["SCALR_EVENT_NAME"] == "BeforeHostUp" or 
            payload["data"]["SCALR_EVENT_NAME"] == "ResumeComplete" or 
            payload["data"]["SCALR_EVENT_NAME"] == "HostDown"):

            if (payload["data"]["SCALR_EVENT_SERVER_HOSTNAME"].lower().endswith(payload["data"]["R53_ZONE"].lower()) ):
                # Use custom server hostname containing R53_ZONE
                a_name = payload["data"]["SCALR_EVENT_SERVER_HOSTNAME"]
            else:
                # Otherwise use cloud ID
                a_name = payload["data"]["SCALR_CLOUD_SERVER_ID"] + '.' + payload["data"]["R53_ZONE"]

            # Make route53 API call
            try:
                conn = route53.connect(
                    aws_access_key_id=payload["data"]["R53_AWS_ACCESS_KEY_ID"],
                    aws_secret_access_key=payload["data"]["R53_AWS_SECRET_ACCESS_KEY"],
                )
                zone = conn.get_hosted_zone_by_id(payload["data"]["R53_ZONE_ID"])
                for record_set in zone.record_sets:
                    # Note Route53 adds trailing period
                    if record_set.name == a_name + '.':
                        record_set.delete()
                        app.logger.info("%s Deleted old Route53 A record: '%s'",
                                        get_logger_timestamp(), a_name)
                        # Stopping early may save some additional HTTP requests,
                        # since zone.record_sets is a generator.
                        break

                # Also create new record on BeforeHostUp or ResumeComplete events
                if (payload["data"]["SCALR_EVENT_NAME"] == "BeforeHostUp" or 
                    payload["data"]["SCALR_EVENT_NAME"] == "ResumeComplete"):
                    new_record, change_info = zone.create_a_record(
                        name = a_name,
                        ttl = 60,
                        values=[payload["data"]["SCALR_EXTERNAL_IP"]],
                    )
                    app.logger.info("%s Created new Route53 A record: '%s' '%s'", get_logger_timestamp(),
                                    a_name, payload["data"]["SCALR_EXTERNAL_IP"])
            except Exception:
                app.logger.exception("%s Route53 API call failed.", get_logger_timestamp())

        else:
            app.logger.info("%s Nothing to do for event '%s'", get_logger_timestamp(),
                            payload["data"]["SCALR_EVENT_NAME"])

    return flask.Response(status=202)


@app.before_request
def log_request():
    app.logger.debug("%s Received request: %s %s", get_logger_timestamp(), flask.request.method,
                     flask.request.url)


@app.before_request
def validate_request_signature():
    if flask.request.method == "GET":
        return

    signing_key = app.config["signing_key"]
    if signing_key is None:
        app.logger.warning("%s No signing key found. Request will not be checked for authenticity.",
                           get_logger_timestamp())
        return

    payload = flask.request.get_data()
    date = flask.request.headers.get("Date", "")
    message_hmac = hmac.HMAC(signing_key, payload + date, hashlib.sha1)

    local_signature = message_hmac.hexdigest()
    remote_signature = flask.request.headers.get("X-Signature", "")

    if not util.constant_time_compare(local_signature, remote_signature):
        app.logger.warning("%s Detected invalid signature, aborting.", get_logger_timestamp())
        return flask.Response(status=403)


@app.before_request
def validate_json_payload():
    if flask.request.method == "GET":
        return
    if flask.request.json is None:
        return flask.Response(status=400)


if __name__ == '__main__':
    app.run(debug=True)
